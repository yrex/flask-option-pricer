

# The maximum number of points calculated in a scenario.
# The cap is in place to avoid excessive calculation load.
MAX_SCENARIO_POINTS = 30
DEFAULT_NUM_SCENARIO_POINTS = 10
CORS_MAX_AGE = 3600



class ReturnTypes(object):
    """
    The type of return values supported by the pricer API.
    This mimics an enum item.
    """

    from models.OptionPricer.CalcType import CalcType

    VALUE, DELTA, GAMMA, THETA, VEGA, RHO = 'VALUE', 'DELTA', 'GAMMA', 'THETA', 'VEGA', 'RHO'
    ALL = [VALUE, DELTA, GAMMA, THETA, VEGA, RHO]
    model_map = {VALUE: CalcType.VALUE,
                 DELTA: CalcType.DELTA,
                 GAMMA: CalcType.GAMMA,
                 THETA: CalcType.THETA,
                 VEGA: CalcType.VEGA,
                 RHO: CalcType.RHO
                 }


class OptionTypes(object):
    CALL, PUT = 'C', 'P'
    ALL = [CALL, PUT]


class PositionTypes(object):
    BUY, SELL = 'B', 'S'
    ALL = [BUY, SELL]


class BSParams(object):

    from models.OptionPricer.X_Type import X_Type

    S, K, D, T, SIGMA, R = 'S', 'K', 'D', 'T', 'SIGMA', 'R'
    ALL = [S, K, D, T, SIGMA, R]
    model_map = {S: X_Type.S,
                 K: X_Type.K,
                 D: X_Type.D,
                 T: X_Type.T,
                 SIGMA: X_Type.SIGMA,
                 R: X_Type.R
                 }

