from flask import Flask
from os import getenv

# import admin
from api.single_leg_pricer import single_leg_pricer_blueprint
from api.multi_leg_pricer import multi_leg_pricer_blueprint
from api.scenario_pricer import scenario_pricer_bluerpint
# from viewer import viewer_blueprint


app = Flask(__name__)
# set Flask config settings
# set a default config, then if a config is set as an environment variable, use it.
# otherwise use production settings.
app.config.from_object('Config.DefaultConfig')
app.config.from_object(getenv('OPTIONAPP_FLASK_CONFIG', 'Config.ProdConfig'))

app.register_blueprint(single_leg_pricer_blueprint)
app.register_blueprint(multi_leg_pricer_blueprint)
app.register_blueprint(scenario_pricer_bluerpint)
# app.register_blueprint(viewer_blueprint)


@app.route('/')
def hello():
    message = "<html><head></head><body>Nothing to see here!</body></html>"
    return message


if __name__ == '__main__':
    app.run(host='0.0.0.0')
