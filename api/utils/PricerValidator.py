from api.constants import ReturnTypes
from api.constants import OptionTypes
from api.constants import PositionTypes
from api.constants import MAX_SCENARIO_POINTS

from functools import reduce

from webargs.core import ValidationError


class PricerValidator(object):
    """
    Define various validation rules used for the Option pricer here, so they can be re-used as necessary.
    """

    @staticmethod
    def validate_return_types(input_types):
        """
        Validate the list contains valid return types (ReturnTypes)
        :param input_types: list of return types

        :return: True if all items are valid ReturnTypes values, otherwise False.
        """

        # check each requested return is a supported return types
        for c in input_types:
            if c not in ReturnTypes.ALL:
                return False
        return True

    @staticmethod
    def validate_list_of_float(input_val):
        return reduce(lambda x, y: x & y, [i >= 0 for i in input_val])

    @staticmethod
    def validate_list_of_float2(input_val):
        return reduce(lambda x, y: x & y, [0 <= i <= 1 for i in input_val])

    @staticmethod
    def validate_list_of_buy_sell(input_val):
        return reduce(lambda x, y: x & y, [s in PositionTypes.ALL for s in input_val])

    @staticmethod
    def validate_list_of_call_put(input_val):
        return reduce(lambda x, y: x & y, [s in OptionTypes.ALL for s in input_val])

    @staticmethod
    def validate_list_len_equal(input_args_dict):

        list_to_validate = list((input_args_dict['price'],
                                input_args_dict['strike'],
                                input_args_dict['dividend'],
                                input_args_dict['time_to_expiry'],
                                input_args_dict['sigma'],
                                input_args_dict['int_rate'],
                                input_args_dict['buy_sell'],
                                input_args_dict['call_put']
                                 ))
        n = len(list_to_validate[0])
        test = all(len(x) == n for x in list_to_validate)
        if test:
            return True
        else:
            raise ValidationError('Invalid number of parameters. Number of input parameters don\'t match.')

    @staticmethod
    def validate_xmin_xmax(input_arg_dict):
        if input_arg_dict['xmin'] < input_arg_dict['xmax']:
            return True
        else:
            raise ValidationError('xmin is larger than xmax.')

    @staticmethod
    def validate_num_scenario_points(input_val):

        if input_val < 2:
            raise ValidationError('Not enough points for scenario.')
        # A maximum number MAX_SCENARIO_POINTS is allowed.
        elif input_val > MAX_SCENARIO_POINTS:
            raise ValidationError('Number of scenario points exceeds {npoints}.'.format(npoints=MAX_SCENARIO_POINTS))
        else:
            return True


def validate_multiple_rules(input_arg_dict):
    """
    Validate multiple PriceValidator rules by looping through each.
    If all rules are satisfied, True is returned.
    If a rule fails, an exception is raised.

    :param args: validation callbacks
    :param kwargs: the input argument dictionary 'input_arg_dict'
    :return: True
    :raise: ValidationError
    """

    validator = PricerValidator()

    rules = [validator.validate_list_len_equal, validator.validate_xmin_xmax]
    try:
        result = True
        for fn in rules:
            result &= fn(input_arg_dict)
        return result
    except ValidationError as err:
        raise err
