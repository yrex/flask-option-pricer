import math

class BaseBSOptionPricer(object):
    """
    A base class definition for a Black-Scholes Option Pricer
    """
    def __init__(self):
        """
        Initialise an instance of the class

        :return: initialise the Option instance
        """
        self.BUY = 'B'
        self.SELL = 'S'

    def value(self, S, K, D, T, sigma, r, buy_sell):
        """
        The value/premium of the Option. This will need to be implemented by classes
        inheriting from this class.

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: not implemented
        """
        raise NotImplementedError

    def delta(self, S, K, D, T, sigma, r, buy_sell):
        """
        The delta of an Option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: not implemented
        """
        raise NotImplementedError

    def gamma(self, S, K, D, T, sigma, r, buy_sell):
        """
        The gamma of an Option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: not implemented
        """
        raise NotImplementedError

    def theta(self, S, K, D, T, sigma, r, buy_sell):
        """
        The theta of an Option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: not implemented
        """
        raise NotImplementedError

    def vega(self, S, K, D, T, sigma, r, buy_sell):
        """
        The vega of an Option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: not implemented
        """
        raise NotImplementedError

    def rho(self, S, K, D, T, sigma, r, buy_sell):
        """
        The rho of an Option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: not implemented
        """
        raise NotImplementedError

    @classmethod
    def _d1(cls, S, K, D, T, sigma, r):
        """
        d1 parameter is BS formula

        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: d1
        """

        # if the time of expiry T=0
        if (T == 0):
            if S < K:
                return float('inf') * -1
            else:
                return float('inf')

        # if the price of strike is set to zero, use a very small value.
        # CDF(1e-20) ~ 1
        if (S == 0):
            S = 1e-20
        if (K == 0):
            K = 1e-20

        # if the volatility is set to zero, use a very small value.
        if sigma == 0:
            sigma = 1e-20

        numerator1 = math.log(S / K)
        numerator2 = (r - D + 0.5 * (sigma**2)) * T
        denominator1 = sigma * math.sqrt(T)

        return (numerator1 + numerator2) / denominator1

    @classmethod
    def _d2(cls, S, K, D, T, sigma, r):
        """
        d2 parameter in BS formula

        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: d2
        """
        return cls._d1(S, K, D, T, sigma, r) - sigma * math.sqrt(T)

    @staticmethod
    def _get_CDF_norm(x):
        """
        wrapper to return the cumulative distribution for a normal distribution

        :param x: probability value
        :return: the cumulative distribution of x
        """

        # approximate the CDF of a normal distribution (mean=0, std=1)
        return 0.5 * (1 + math.erf(x / math.sqrt(2)))

    @staticmethod
    def _get_CDF_norm_derivative(x):
        """
        the derivative of the CDF of a normal distribution

        :param x: probability value
        :return: the derivative f the cumulative distribution of x
        """
        return (1 / math.sqrt(2 * math.pi)) * math.exp(-0.5 * (x**2))

    @staticmethod
    def _check_pricer_inputs(S, K, T, sigma):
        """
        Check that the pricer input values are sensible

        :param S: underlying asset price
        :param K: Option strike price
        :param T: time to expiry
        :param sigma: the volatility of the underlying

        :return: raises an exception if errors are found. Otherwise returns true
        """

        if S < 0:
            raise ValueError('Asset price is less than zero.')

        if K < 0:
            raise ValueError('Strike price is less than zero.')

        if T < 0:
            raise ValueError('Time to Expiry is less than zero.')

        if sigma < 0:
            raise ValueError('Volatility is less than zero.')

        return True

    def _buy_sell_sign(self, buy_sell):
        """
        Adjust the value or greek for buy/sell.

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :return: 1 if Buy, -1 if Sell.
        """

        # get the first character, in case 'buy' or 'sell' were input
        b_s = buy_sell[0].upper()

        if b_s == self.BUY:
            return 1
        elif b_s == self.SELL:
            return -1
        else:
            raise ValueError('Invalid Buy/Sell flag.')

if __name__ == '__main__':
    pass