import unittest

from models.OptionPricer.EuropeanBSCallOptionPricer import EuropeanBSCallOptionPricer


class TestEuroBSCall(unittest.TestCase):
    def test_Pricer_instance(self):
        P = EuropeanBSCallOptionPricer()
        self.assertIsInstance(P, EuropeanBSCallOptionPricer)

    def test_Pricer_d1(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P._d1(100, 100, 0, 1, 0.2, 0.05), 0.35, 6)

    def test_Pricer_d2(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P._d2(100, 100, 0, 1, 0.2, 0.05), 0.15, 6)

    def test_Pricer_value(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.value(100, 100, 0, 1, 0.2, 0.05, 'B'), 10.4506, 4)

    def test_Pricer_value_T0(self):
        P = EuropeanBSCallOptionPricer()
        # T = 0
        self.assertAlmostEqual(P.value(110, 100, 0, 0, 0.2, 0.05, 'B'), 10, 4)

    def test_Pricer_value_S0(self):
        P = EuropeanBSCallOptionPricer()
        # S = 0
        self.assertAlmostEqual(P.value(0, 100, 0, 1, 0.2, 0.05, 'B'), 0, 4)

    def test_Pricer_value_K0(self):
        P = EuropeanBSCallOptionPricer()
        # K = 0
        self.assertAlmostEqual(P.value(100, 0, 0, 1, 0.2, 0.05, 'B'), 100, 4)

    def test_Pricer_value_Sneg(self):
        P = EuropeanBSCallOptionPricer()
        # S < 0
        with self.assertRaises(ValueError):
            P.value(-100, 100, 0, 1, 0.2, 0.05, 'B')

    def test_Pricer_delta(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.delta(100, 100, 0, 1, 0.2, 0.05, 'B'), 0.6368, 4)

    def test_Pricer_gamma(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.gamma(100, 100, 0, 1, 0.2, 0.05, 'B'), 0.0188, 4)

    def test_Pricer_theta(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.theta(100, 100, 0, 1, 0.2, 0.05, 'B'), -6.4140, 4)

    def test_Pricer_vega(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.vega(100, 100, 0, 1, 0.2, 0.05, 'B'), 37.524, 4)

    def test_Pricer_rho(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.rho(100, 100, 0, 1, 0.2, 0.05, 'B'), 53.2325, 4)

    def test_Pricer_rho_sell(self):
        P = EuropeanBSCallOptionPricer()
        self.assertAlmostEqual(P.rho(100, 100, 0, 1, 0.2, 0.05, 'S'), -53.2325, 4)

if __name__ == '__main__':
    unittest.main()
