from flask import Blueprint
from flask import jsonify
from flask.ext.cors import CORS

from webargs import Arg
from webargs.flaskparser import use_args

from models.OptionPricer.OptionScenario import OptionScenario

from api.utils.PricerValidator import PricerValidator
from api.utils.PricerValidator import validate_multiple_rules
from api.utils.ArgConversions import ArgConversions
from api.constants import ReturnTypes
from api.constants import BSParams
from api.constants import DEFAULT_NUM_SCENARIO_POINTS


scenario_pricer_bluerpint = Blueprint('scenario_pricer', __name__, url_prefix='/api')

# enable CORS on the blueprint
CORS(scenario_pricer_bluerpint, max_age=3600, methods=['GET'])


# list of input arguments that need to be validated
black_scholes_args = {
    'price':            Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid price'),

    'strike':           Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid strike'),

    'dividend':         Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid dividend'),

    'time_to_expiry':   Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid time to expiry'),

    'sigma':            Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float2, error='Invalid sigma'),

    'int_rate':         Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float2, error='Invalid interest rate'),

    'buy_sell':         Arg(list, required=True, use=ArgConversions.use_list_of_str,
                            validate=PricerValidator.validate_list_of_buy_sell, error='Invalid Buy/Sell'),

    'call_put':         Arg(list, required=True, use=ArgConversions.use_list_of_str,
                            validate=PricerValidator.validate_list_of_call_put, error='Invalid Call/Put'),

    'y_type':           Arg(str, required=True, use=lambda s: s.upper(), validate=lambda s: s in ReturnTypes.ALL,
                            error='Invalid y_type'),

    'x_type':           Arg(str, required=True, use=lambda s: s.upper(), validate=lambda s: s in BSParams.ALL,
                            error='Invalid x_type'),

    'xmin':             Arg(float, required=True, error='Invalid xmin'),

    'xmax':             Arg(float, required=True, error='Invalid xmax'),

    'num_points':       Arg(int, default=DEFAULT_NUM_SCENARIO_POINTS, validate=PricerValidator.validate_num_scenario_points,
                            error='Invalid number of points'),

    'time_offset':      Arg(float, default=0, validate=lambda t: t >= 0, error='Invalid time offset'),

    'include_original_x_vals':
                        Arg(bool, default=False, use=ArgConversions.use_bool, error='Invalid include_original_x_vals'),

    'add_support_points':
                        Arg(bool, default=False, use=ArgConversions.use_bool, error='Invalid add_support_points')

}


@scenario_pricer_bluerpint.errorhandler(422)
def handle_validation_error(err):
    exc = err.data['exc']
    return jsonify({'message': str(exc)}), 400


@scenario_pricer_bluerpint.route('/pricer/scenario/black_scholes', methods=['GET'])
@use_args(black_scholes_args, validate=validate_multiple_rules)
def scenario_pricer(args):
    """
    calculate a scenario for an option strategy based on the Black Scholes formula.
    arguments for the legs are given as lists.

    Example:

    api/pricer/scenario/black_scholes?price=100,100&strike=100,100&dividend=0,0&time_to_expiry=1,1&sigma=0.2,0.2&
                                       int_rate=0.05,0.05&buy_sell=B,S&call_put=C,P&
                                       ret_types=value,delta,gamma,vega,theta,rho&
                                       y_type=value&x_type=S&xmin=90&xmax=110&num_points=10&time_offset=0&
                                       include_original_x_vals=0&add_support_points=0

    Parameters
    :parameter list price: the underlying asset price
    :parameter list strike: the strike price
    :parameter list dividend: dividend yield, given in decimals
    :parameter list time_to_expiry: the option time to expiry, given in years
    :parameter list sigma: volatility of the underlying asset, given in decimals
    :parameter list int_rate: interest rate, given in decimals
    :parameter list buy_sell: option buy or sell. given as 'b' or 's'. Uppercase form is also valid.
    :parameter list call_put: option call or put. given as 'c' or 'p'. Uppercase form is also valid.
    :parameter str y_type: value being calculated, of type ReturnTypes.
    :parameter str x_type: value modified in scenario, of type BSParams.
    :parameter float xmin: the minimum value in scenario.
    :parameter float xmax: the maximum value in scenario.
    :parameter int num_points: the number of points in scenario. A maximum number MAX_SCENARIO_POINTS is allowed.
    :parameter float time_offset: time epoch to calculate value/greek of Option scenario.
    :parameter bool include_original_x_vals: whether to include the original x-axis parameter. Default is False.
    :parameter bool add_support_points: whether to include extra support points on either side of the original
                                        x-axis parameter. The default is False. If True, include_original_x_vals
                                        must be True as well, otherwise it is ignored.


    :return: JSON response
    """

    # strategy_list, y_type, x_type, xmin, xmax, num_points=10, time_offset=0,
    #               include_original_x_vals=False, add_support_points=False

    # Construct the input parameters to the model like so:
    # [ ('C', [S, K, D, T, sigma, r, buy_sell]),
    #   ('P', [S, K, D, T, sigma, r, buy_sell])]

    arg_list = zip(
        args['price'],
        args['strike'],
        args['dividend'],
        args['time_to_expiry'],
        args['sigma'],
        args['int_rate'],
        args['buy_sell']
    )

    # convert the zip tuples to a list
    arg_list = [list(z) for z in arg_list]

    arg_list = [v for v in zip(args['call_put'], arg_list)]

    # results dictionary
    results = dict()
    results['ERROR'] = list()

    OptScen = OptionScenario()

    # map the api x_type and y_type to the variant used by the model
    x_type = BSParams.model_map[args['x_type']]
    y_type = ReturnTypes.model_map[args['y_type']]

    try:
        scen_total, scen_legs, xvals = OptScen.calculate(arg_list, y_type, x_type, args['xmin'], args['xmax'],
                                                         args['num_points'], args['time_offset'],
                                                         args['include_original_x_vals'], args['add_support_points'])
        results['SCENARIO_TOTAL'] = scen_total
        results['SCENARIO_LEGS'] = scen_legs
        results['X_VALUES'] = xvals

    except:
        results['ERROR'].append('Error calculating scenario')

    return jsonify(results)
