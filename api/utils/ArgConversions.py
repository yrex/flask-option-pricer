from webargs.core import ValidationError


class ArgConversions(object):
    """
    Define various argument conversion methods, that can be re-used as necessary.
    """

    @staticmethod
    def use_list_of_float(input_val):
        try:
            return [float(v) for v in input_val.replace(' ', '').split(',')]
        except:
            raise ValidationError('Invalid values.')

    @staticmethod
    def use_list_of_str(input_val):
        try:
            return [v.upper() for v in input_val.replace(' ', '').split(',')]
        except:
            raise ValidationError('Invalid values.')

    @staticmethod
    def use_bool(input_val):
        """
        Convert string, integer and float values to
        """
        try:
            if isinstance(input_val, str):
                if input_val.upper() in ('Y', '1', 'TRUE'):
                    return True
                else:
                    return False
            elif isinstance(input_val, (int, float)):
                return bool(input_val)
            else:
                raise ValidationError('Invalid boolean')
        except:
            raise ValidationError('Invalid boolean')
