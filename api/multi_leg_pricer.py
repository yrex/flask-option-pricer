from flask import Blueprint
from flask import jsonify
from flask.ext.cors import CORS

from webargs import Arg
from webargs.flaskparser import use_args

from models.OptionPricer.OptionStrategy import OptionStrategy

from api.constants import ReturnTypes
from api.utils.PricerValidator import PricerValidator
from api.utils.ArgConversions import ArgConversions
from api.constants import CORS_MAX_AGE


multi_leg_pricer_blueprint = Blueprint('multi_leg_pricer', __name__, url_prefix='/api')

# enable CORS on the blueprint
CORS(multi_leg_pricer_blueprint, max_age=CORS_MAX_AGE, methods=['GET'])


# list of input arguments that need to be validated
black_scholes_args = {
    'price':            Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid price'),

    'strike':           Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid strike'),

    'dividend':         Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid dividend'),

    'time_to_expiry':   Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float, error='Invalid time to expiry'),

    'sigma':            Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float2, error='Invalid sigma'),

    'int_rate':         Arg(list, required=True, use=ArgConversions.use_list_of_float,
                            validate=PricerValidator.validate_list_of_float2, error='Invalid interest rate'),

    'buy_sell':         Arg(list, required=True, use=ArgConversions.use_list_of_str,
                            validate=PricerValidator.validate_list_of_buy_sell, error='Invalid Buy/Sell'),

    'call_put':         Arg(list, required=True, use=ArgConversions.use_list_of_str,
                            validate=PricerValidator.validate_list_of_call_put, error='Invalid Call/Put'),

    'ret_types':        Arg(list, required=True, use=lambda s: s.replace(' ', '').upper().split(','),
                            validate=PricerValidator.validate_return_types, error='Invalid return type')
}


@multi_leg_pricer_blueprint.errorhandler(422)
def handle_validation_error(err):
    try:
        exc = err.data['exc']
    except:
        exc = err.description
    return jsonify({'message': str(exc)}), 400


@multi_leg_pricer_blueprint.route('/pricer/multi_leg/black_scholes', methods=['GET'])
@use_args(black_scholes_args, validate=PricerValidator.validate_list_len_equal)
def multi_leg_pricer(args):
    """
    calculate the price and greeks for a multi-leg option strategy based on the Black Scholes formula.
    arguments for the legs are given as lists.

    Example:

    api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100&dividend=0,0&time_to_expiry=1,1&
                                       sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&
                                       ret_types=value,delta,gamma,vega,theta,rho

    Parameters
    :parameter list price: the underlying asset price
    :parameter list strike: the strike price
    :parameter list dividend: dividend yield, given in decimals
    :parameter list time_to_expiry: the option time to expiry, given in years
    :parameter list sigma: volatility of the underlying asset, given in decimals
    :parameter list int_rate: interest rate, given in decimals
    :parameter list buy_sell: option buy or sell. given as 'b' or 's'. Uppercase form is also valid.
    :parameter list call_put: option call or put. given as 'c' or 'p'. Uppercase form is also valid.
    :parameter list ret_types: a comma separated string with the calculations to return

    :return: JSON response
    """

    # Construct the input parameters to the model like so:
    # [ ('C', [S, K, D, T, sigma, r, buy_sell]),
    #   ('P', [S, K, D, T, sigma, r, buy_sell])]

    arg_list = list(zip(
        args['price'],
        args['strike'],
        args['dividend'],
        args['time_to_expiry'],
        args['sigma'],
        args['int_rate'],
        args['buy_sell']
    ))

    arg_list = [v for v in zip(args['call_put'], arg_list)]

    # results dictionary
    results = dict()
    results['ERROR'] = list()

    OptStrat = OptionStrategy()

    for ret_type in args['ret_types']:

        if ret_type == ReturnTypes.VALUE:
            try:
                results[ret_type] = OptStrat.value(arg_list)
            except ValueError:
                results[ret_type] = None
                results['ERROR'].append('Error calculating value')
        elif ret_type == ReturnTypes.DELTA:
            try:
                results[ret_type] = OptStrat.delta(arg_list)
            except ValueError:
                results[ret_type] = None
                results['ERROR'].append('Error calculating delta')
        elif ret_type == ReturnTypes.GAMMA:
            try:
                results[ret_type] = OptStrat.gamma(arg_list)
            except ValueError:
                results[ret_type] = None
                results['ERROR'].append('Error calculating gamma')
        elif ret_type == ReturnTypes.THETA:
            try:
                results[ret_type] = OptStrat.theta(arg_list)
            except ValueError:
                results[ret_type] = None
                results['ERROR'].append('Error calculating theta')
        elif ret_type == ReturnTypes.VEGA:
            try:
                results[ret_type] = OptStrat.vega(arg_list)
            except ValueError:
                results[ret_type] = None
                results['ERROR'].append('Error calculating vega')
        elif ret_type == ReturnTypes.RHO:
            try:
                results[ret_type] = OptStrat.rho(arg_list)
            except ValueError:
                results[ret_type] = None
                results['ERROR'].append('Error calculating rho')
        else:
            results['ERROR'].append('Incorrect return type provided')

    return jsonify(results)
