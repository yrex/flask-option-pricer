import unittest

from models.OptionPricer.BaseBSOptionPricer import BaseBSOptionPricer


class TestBaseEuroBSPricer(unittest.TestCase):
    def test_Pricer_instance(self):
        P = BaseBSOptionPricer()
        self.assertIsInstance(P, BaseBSOptionPricer)

    def test_d1(self):
        P = BaseBSOptionPricer()
        self.assertAlmostEqual(P._d1(100, 100, 0, 1, 0.2, 0.05), 0.35, 6)

    def test_d1_2(self):
        P = BaseBSOptionPricer()
        self.assertAlmostEqual(P._d1(100, 120, 0, 1, 0.2, 0.05), -0.561608, 6)

    def test_d1_3(self):
        P = BaseBSOptionPricer()
        self.assertAlmostEqual(P._d1(100, 120, 0.01, 3, 0.2, 0.05), -0.006702, 6)

    def test_d1_4(self):
        P = BaseBSOptionPricer()
        self.assertEqual(P._d1(100, 120, 0, 0, 0.2, 0.05), float('-inf'))

    def test_d2(self):
        P = BaseBSOptionPricer()
        self.assertAlmostEqual(P._d2(100, 120, 0.01, 3, 0.2, 0.05), -0.353111919, 6)

    def test_d2_2(self):
        P = BaseBSOptionPricer()
        self.assertEqual(P._d2(100, 120, 0.01, 0, 0.2, 0.05), float('-inf'))

    def test_check_pricer_input(self):
        P = BaseBSOptionPricer()
        self.assertTrue(P._check_pricer_inputs(100, 120, 0.01, 3))

    def test_check_pricer_input_Sneg(self):
        P = BaseBSOptionPricer()
        with self.assertRaises(ValueError):
            P._check_pricer_inputs(-2, 120, 0.01, 3)

    def test_check_pricer_input_Kneg(self):
        P = BaseBSOptionPricer()
        with self.assertRaises(ValueError):
            P._check_pricer_inputs(100, -120, 0.01, 3)

    def test_check_pricer_input_Tneg(self):
        P = BaseBSOptionPricer()
        with self.assertRaises(ValueError):
            P._check_pricer_inputs(100, 120, -0.01, 3)

    def test_check_pricer_input_Sigmaneg(self):
        P = BaseBSOptionPricer()
        with self.assertRaises(ValueError):
            P._check_pricer_inputs(100, 120, 0.01, -3)

    def test_check_pricer_input_someneg(self):
        P = BaseBSOptionPricer()
        with self.assertRaises(ValueError):
            P._check_pricer_inputs(-100, 120, -0.01, -3)

    def test_buy_sell_sign(self):
        P = BaseBSOptionPricer()
        self.assertEqual(P._buy_sell_sign('B'), 1)

    def test_buy_sell_sign2(self):
        P = BaseBSOptionPricer()
        self.assertEqual(P._buy_sell_sign('b'), 1)

    def test_buy_sell_sign3(self):
        P = BaseBSOptionPricer()
        self.assertEqual(P._buy_sell_sign('buy'), 1)

    def test_buy_sell_sign4(self):
        P = BaseBSOptionPricer()
        self.assertEqual(P._buy_sell_sign('S'), -1)

    def test_buy_sell_sign5(self):
        P = BaseBSOptionPricer()
        with self.assertRaises(ValueError):
            P._buy_sell_sign('X')


if __name__ == '__main__':
    unittest.main()
