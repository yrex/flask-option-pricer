import unittest
import json
import calculator


class testMultiLegPricer(unittest.TestCase):

    def setUp(self):
        self.app = calculator.app.test_client()

    def tearDown(self):
        pass

    def test_api_no_param(self):
        """
        Test that accessing the API without any parameters returns an error status code.
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes')
        self.assertEqual(response.status_code, 400)
        self.assertIn(b'Required parameter', response.data)
        self.assertIn(b'not found', response.data)

    def test_api_request_value(self):
        """
        Test that requesting the Value returns the correct number
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_response['ERROR'], [])
        self.assertAlmostEqual(json_response['VALUE'][0], 16.024110, 6)
        self.assertAlmostEqual(json_response['VALUE'][0], 16.024110, 6)
        self.assertAlmostEqual(json_response['VALUE'][1][0], 10.450584, 6)
        self.assertAlmostEqual(json_response['VALUE'][1][1], 5.573526, 6)
        self.assertEqual(len(json_response.keys()), 2)
        # print(json_response)

    def test_api_request_invalid_return_type(self):
        """
        Test that requesting an invalid return type returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=INVALIDvalue')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid return type')

    def test_api_request_invalid_call_put(self):
        """
        Test that requesting an invalid call/put returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,X&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid Call/Put')

    def test_api_request_missing_call_put(self):
        """
        Test that requesting an invalid call/put returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid Call/Put')

    def test_api_request_string_call_put(self):
        """
        Test that requesting an invalid call/put returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'],
                         'Invalid number of parameters. Number of input parameters don\'t match.')

    def test_api_pass_string_values(self):
        """
        Test that passing a string value instead of numerals returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,QQQ&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid values.')

    def test_api_missing_price(self):
        """
        Test that not passing the price argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "price" not found.')

    def test_api_missing_strike(self):
        """
        Test that not passing the strike argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "strike" not found.')

    def test_api_missing_dividend(self):
        """
        Test that not passing the dividend argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "dividend" not found.')

    def test_api_missing_expiry(self):
        """
        Test that not passing the time_to_expiry argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "time_to_expiry" not found.')

    def test_api_missing_sigma(self):
        """
        Test that not passing the sigma argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "sigma" not found.')

    def test_api_missing_int_rate(self):
        """
        Test that not passing the int_rate argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "int_rate" not found.')

    def test_api_missing_buy_sell(self):
        """
        Test that not passing the buy_sell argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "buy_sell" not found.')

    def test_api_missing_call_put(self):
        """
        Test that not passing the call_put argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "call_put" not found.')

    def test_api_missing_ret_types(self):
        """
        Test that not passing the ret_types argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "ret_types" not found.')

    def test_api_invalid_buy_sell(self):
        """
        Test that not passing an invalid buy_sell argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=X,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid Buy/Sell')

    def test_api_negative_expirty(self):
        """
        Test that not passing a negative time_to_expiry argument returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=-1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid time to expiry')

    def test_api_large_sigma(self):
        """
        Test that not passing a sigma larger than 1 returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,1.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid sigma')

    def test_api_negative_sigma(self):
        """
        Test that not passing a negative sigma returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,-0.2&int_rate=0.05,0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid sigma')

    def test_api_large_int_rate(self):
        """
        Test that not passing a int_rate larger than 1 returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,1.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid interest rate')

    def test_api_negative_int_rate(self):
        """
        Test that not passing a negative int_rate returns an error message
        """
        response = self.app.get('api/pricer/multi_leg/black_scholes?price=100,100&strike=100,100'
                                '&dividend=0,0&time_to_expiry=1,1&'
                                'sigma=0.2,0.2&int_rate=0.05,-0.05&buy_sell=B,B&call_put=C,P&'
                                'ret_types=value')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid interest rate')

if __name__ == '__main__':
    unittest.main()
