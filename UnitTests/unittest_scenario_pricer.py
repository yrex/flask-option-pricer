import unittest
import json
import calculator


class testScenarioPricer(unittest.TestCase):

    def setUp(self):
        self.app = calculator.app.test_client()

    def tearDown(self):
        pass

    def test_api_no_param(self):
        """
        Test that accessing the API without any parameters returns an error status code.
        """
        response = self.app.get('api/pricer/scenario/black_scholes')
        self.assertEqual(response.status_code, 400)
        self.assertIn(b'Required parameter', response.data)
        self.assertIn(b'not found', response.data)

    def test_api_request_value(self):
        """
        Test that requesting the Value returns the correct number
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=0')
        json_response = json.loads(response.data.decode('ascii'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_response['ERROR'], [])
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][0], 5.091222, 6)
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][1], -10.214165, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][0], -5.122942, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][1], 14.877058, 6)
        self.assertEqual(json_response['X_VALUES'], [90, 110])
        self.assertEqual(len(json_response.keys()), 4)
        # print(json_response)

    def test_api_request_invalid_return_type(self):
        """
        Test that requesting an invalid return type returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=INVALIDvalue&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=0')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid y_type')

    def test_api_request_value_inc_original(self):
        """
        Test that requesting value with include_original_x_vals returns correct results
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=1&add_support_points=0')
        json_response = json.loads(response.data.decode('ascii'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_response['ERROR'], [])
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][0], 5.091222, 6)
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][1], -10.214165, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][0], -5.122942, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][1], 4.877058, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][2], 14.877058, 6)
        self.assertEqual(json_response['X_VALUES'], [90, 100, 110])
        self.assertEqual(len(json_response['SCENARIO_TOTAL']), 3)
        self.assertEqual(len(json_response.keys()), 4)

    def test_api_request_value_inc_original_with_support_point(self):
        """
        Test that requesting value with include_original_x_vals and support points returns correct results
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=1&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_response['ERROR'], [])
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][0], 5.091222, 6)
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][1], -10.214165, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][0], -5.122942, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][1], 3.877058, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][2], 4.877058, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][3], 5.877058, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][4], 14.877058, 6)
        self.assertEqual(json_response['X_VALUES'], [90, 99, 100, 101, 110])
        self.assertEqual(len(json_response['SCENARIO_TOTAL']), 5)
        self.assertEqual(len(json_response.keys()), 4)

    def test_api_request_with_support_point_wihtout_value_inc_original_(self):
        """
        Test that requesting value support points and without include_original_x_vals returns correct results
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json_response['ERROR'], [])
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][0], 5.091222, 6)
        self.assertAlmostEqual(json_response['SCENARIO_LEGS'][0][1], -10.214165, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][0], -5.122942, 6)
        self.assertAlmostEqual(json_response['SCENARIO_TOTAL'][1], 14.877058, 6)
        self.assertEqual(json_response['X_VALUES'], [90, 110])
        self.assertEqual(len(json_response['SCENARIO_TOTAL']), 2)
        self.assertEqual(len(json_response.keys()), 4)

    def test_api_request_string_call_put(self):
        """
        Test that requesting an invalid call/put returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'],
                         'Invalid number of parameters. Number of input parameters don\'t match.')

    def test_api_pass_string_values(self):
        """
        Test that passing a string value instead of numerals returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,QQQ'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid values.')

    def test_api_missing_price(self):
        """
        Test that not passing the price argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "price" not found.')

    def test_api_missing_strike(self):
        """
        Test that not passing the strike argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "strike" not found.')

    def test_api_missing_dividend(self):
        """
        Test that not passing the dividend argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "dividend" not found.')

    def test_api_missing_expiry(self):
        """
        Test that not passing the time_to_expiry argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "time_to_expiry" not found.')

    def test_api_missing_sigma(self):
        """
        Test that not passing the sigma argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "sigma" not found.')

    def test_api_missing_int_rate(self):
        """
        Test that not passing the int_rate argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "int_rate" not found.')

    def test_api_missing_buy_sell(self):
        """
        Test that not passing the buy_sell argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "buy_sell" not found.')

    def test_api_missing_call_put(self):
        """
        Test that not passing the call_put argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "call_put" not found.')

    def test_api_missing_ret_types(self):
        """
        Test that not passing the y_types argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Required parameter "y_type" not found.')

    def test_api_invalid_buy_sell(self):
        """
        Test that not passing an invalid buy_sell argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,X'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid Buy/Sell')

    def test_api_negative_expirty(self):
        """
        Test that not passing a negative time_to_expiry argument returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,-1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid time to expiry')

    def test_api_large_sigma(self):
        """
        Test that not passing a sigma larger than 1 returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,1.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid sigma')

    def test_api_negative_sigma(self):
        """
        Test that not passing a negative sigma returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=-0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid sigma')

    def test_api_large_int_rate(self):
        """
        Test that not passing a int_rate larger than 1 returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,1.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid interest rate')

    def test_api_negative_int_rate(self):
        """
        Test that not passing a negative int_rate returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=-0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=2&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Invalid interest rate')

    def test_api_negative_num_points(self):
        """
        Test that not passing a negative num_points returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=-1&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Not enough points for scenario.')

    def test_api_zero_num_points(self):
        """
        Test that not passing a negative num_points returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&num_points=0&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Not enough points for scenario.')

    def test_api_default_num_points(self):
        """
        Test that not passing num_points, results in a default value being used
        """

        from api.constants import DEFAULT_NUM_SCENARIO_POINTS

        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=0')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json_response['X_VALUES']), DEFAULT_NUM_SCENARIO_POINTS)

    def test_api_over_limit_num_points(self):
        """
        Test that not passing a num_points over the set limit returns an error message
        """

        from api.constants import MAX_SCENARIO_POINTS

        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=90&xmax=110'
                                '&num_points=31'
                                '&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'Number of scenario points exceeds 30.')

    def test_api_xmin_xmax_order(self):
        """
        Test that not an incorrect order of xmin and xmax returns an error message
        """
        response = self.app.get('api/pricer/scenario/black_scholes?price=100,100'
                                '&strike=100,100&dividend=0,0&time_to_expiry=1,1'
                                '&sigma=0.2,0.2&int_rate=0.05,0.05&buy_sell=B,S'
                                '&call_put=C,P'
                                '&y_type=value&x_type=S&xmin=110&xmax=90&num_points=4&time_offset=0'
                                '&include_original_x_vals=0&add_support_points=1')
        json_response = json.loads(response.data.decode('ascii'))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json_response['message'], 'xmin is larger than xmax.')

if __name__ == '__main__':
    unittest.main()
