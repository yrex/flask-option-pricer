

class X_Type(object):
    """
    The type of values used in scenarios
    """

    S, K, D, T, SIGMA, R = range(1, 7)