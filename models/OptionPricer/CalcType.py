

class CalcType(object):
    """
    The type of calculation to carry out.
    This mimics an enum item.
    """

    VALUE, DELTA, GAMMA, THETA, VEGA, RHO = range(1, 7)