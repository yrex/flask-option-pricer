A Flask-based REST API for the financial European style option pricing and risk calcluation.
Note that this implementation uses the Black-Scholes pricing method and is intended as an educational tool, and is not fit for real-life options trading!
Two methods are available:

### Pricer
The API can perform calculations on single call/put options or on multiple contracts, forming an _option strategies_.
The inputs consist of the underlying price, required strike, volatility, interest rate, time to maturity and dividend.
The output, which is requested in the message, is price, delta, gamma, vega, theta, rho.

### Profiling
With this API, the behaviour of a given option strategy can be studied. The scenario analysis method will vary a given input
parameter and produce the results for the chosen output. The calculation time can also be specified, so the scenario can be
calculated at specific times (before) expiry.

For example, to calculate the payoff of a given
strategy, the underlying price is varied over a given range and the price is chosen as the output. Similarly, the effect
of time on the vega of a strategy can be calculated.

### Viewing Results
The calculation results are returned in JSON format.
A web-based [Option Pricer and Risk Profiler](https://optionprofiler.com) is also available for interacting and performing calculations.
You can also access the code for that website [here](https://gitlab.com/yrex/option-pricer-viewer).


### Getting Started
Clone this repository in a local directory, then install the required Python modules. It's best to use a virtual environment.
You can do this by creating a new directory in your local project directory
```
mkdir virtEnv
```
then creating a virtual environment there:
```
cd virtEnv
virtualenv ve_option_api
```
and enabling it
```
source ve_option_api/bin/activate
```
(_instructions slightly different on Windows_)

Then install the dependencies
```
pip install -r ../flask-option-pricer/requirements.txt
```

You can then enable a local Python server and launch `calculator.py`. The server will then listen to requests to the API.

**TODO** Add API implementation details.
