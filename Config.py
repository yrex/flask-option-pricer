

class DefaultConfig(object):
    DEBUG = False
    TESTING = False

class DevConfig(DefaultConfig):
    DEBUG = True

class ProdConfig(DefaultConfig):
    DEBUG = False

