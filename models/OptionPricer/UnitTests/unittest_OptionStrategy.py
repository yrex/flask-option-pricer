import unittest

from models.OptionPricer.OptionStrategy import OptionStrategy
from models.OptionPricer.X_Type import X_Type


class TestOptionStrategy(unittest.TestCase):
    def test_Strategy_instance(self):
        s = OptionStrategy()
        self.assertIsInstance(s, OptionStrategy)

    def test_1leg_value(self):
        s = OptionStrategy()
        leg = [('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])]
        tot, result = s.value(leg)
        self.assertAlmostEqual(tot, 10.4506, 4)
        self.assertAlmostEqual(result[0], 10.4506, 4)

    def test_2leg_value(self):
        s = OptionStrategy()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        strat = [leg1, leg2]
        tot, result = s.value(strat)
        self.assertAlmostEqual(tot, 10.4506*2, 4)
        self.assertAlmostEqual(result[0], 10.4506, 4)
        self.assertAlmostEqual(result[1], 10.4506, 4)

    def test_2leg_value_2(self):
        s = OptionStrategy()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]
        tot, result = s.value(strat)
        self.assertAlmostEqual(tot, 10.4506 + -5.5735, 4)
        self.assertAlmostEqual(result[0], 10.4506, 4)
        self.assertAlmostEqual(result[1], -5.5735, 4)

    def test_get_strategy(self):
        s = OptionStrategy()
        leg1 = ('C', [90, 120, 0, 1, 0.2, 0.05, 'B'])

        p = s.get_strategy_param(leg1, X_Type.S)

        self.assertEqual(p, 90)

    def test_get_strategy2(self):
        s = OptionStrategy()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])

        p = s.get_strategy_param(leg1, X_Type.R)

        self.assertEqual(p, 0.05)

    def test_update_strategy(self):
        s = OptionStrategy()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg1_new = ('C', [120, 100, 0, 1, 0.2, 0.05, 'B'])

        p = s.update_strategy(leg1, X_Type.S, 120)

        self.assertEqual(leg1_new, p)


if __name__ == '__main__':
    unittest.main()
