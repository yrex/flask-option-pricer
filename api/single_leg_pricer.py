from flask import Blueprint
from flask import jsonify

from webargs import Arg
from webargs.flaskparser import use_args

from models.OptionPricer.EuropeanBSCallOptionPricer import EuropeanBSCallOptionPricer
from models.OptionPricer.EuropeanBSPutOptionPricer import EuropeanBSPutOptionPricer

from api.utils.PricerValidator import PricerValidator
from api.constants import ReturnTypes
from api.constants import OptionTypes
from api.constants import PositionTypes

# list of input arguments that need to be validated
black_scholes_args = {
    'price':            Arg(float, required=True, validate=lambda x: x >= 0, error='Invalid price'),

    'strike':           Arg(float, required=True, validate=lambda x: x >= 0, error='Invalid strike'),

    'dividend':         Arg(float, required=True, validate=lambda x: x >= 0, error='Invalid dividend'),

    'time_to_expiry':   Arg(float, required=True, validate=lambda x: x >= 0, error='Invalid time to expiry'),

    'sigma':            Arg(float, required=True, validate=lambda x: 0 <= x <= 1, error='Invalid sigma'),

    'int_rate':         Arg(float, required=True, validate=lambda x: 0 <= x <= 1, error='Invalid interest rate'),

    'buy_sell':         Arg(str, required=True, use=lambda s: s.upper(),
                            validate=lambda s: s.upper() in PositionTypes.ALL, error='Invalid Buy/Sell'),

    'call_put':         Arg(str, required=True, use=lambda s: s.upper(),
                            validate=lambda s: s.upper() in OptionTypes.ALL, error='Invalid Call/Put'),

    'ret_types':        Arg(list, required=True, use=lambda s: s.replace(' ', '').upper().split(','),
                            validate=PricerValidator.validate_return_types, error='Invalid return type')
}

single_leg_pricer_blueprint = Blueprint('single_leg_pricer', __name__, url_prefix='/api')


@single_leg_pricer_blueprint.errorhandler(422)
def handle_validation_error(err):
    exc = err.data['exc']
    return jsonify({'message': str(exc)}), 400


@single_leg_pricer_blueprint.route('/pricer/single_leg/black_scholes', methods=['GET'])
@use_args(black_scholes_args)
def single_leg_pricer(args):

    """
    calculate the price and greeks for an option based on the Black Scholes formula.

    example:

    api/pricer/black_scholes?price=100&strike=100&dividend=0&time_to_expiry=1&
                             sigma=0.2&int_rate=0.05&buy_sell=B&call_put=C&
                             ret_types=value,delta,gamma,vega,theta,rho

    Parameters
    :parameter price: the underlying asset price
    :parameter strike: the strike price
    :parameter dividend: dividend yield, given in decimals
    :parameter time_to_expiry: the option time to expiry, given in years
    :parameter sigma: volatility of the underlying asset, given in decimals
    :parameter int_rate: interest rate, given in decimals
    :parameter buy_sell: option buy or sell. given as 'b' or 's'. Uppercase form is also valid.
    :parameter call_put: option call or put. given as 'c' or 'p'. Uppercase form is also valid.
    :parameter ret_types: a comma separated string with the calculations to return

    :return: JSON response
    """

    arg_list = [
        args['price'],
        args['strike'],
        args['dividend'],
        args['time_to_expiry'],
        args['sigma'],
        args['int_rate'],
        args['buy_sell']
    ]

    results = dict()
    results['ERROR'] = list()

    # Get the correct pricer
    if args['call_put'] == OptionTypes.CALL:
        opt_pricer = EuropeanBSCallOptionPricer()
    elif args['call_put'] == OptionTypes.PUT:
        opt_pricer = EuropeanBSPutOptionPricer()
    else:
        results['ERROR'].append('incorrect option type')
        return str(results)

    for ret_type in args['ret_types']:

        if ret_type == ReturnTypes.VALUE:
            try:
                results[ret_type] = opt_pricer.value(*arg_list)
            except ValueError:
                results[ret_type] = 0
                results['ERROR'].append('Error calculating value')
        elif ret_type == ReturnTypes.DELTA:
            try:
                results[ret_type] = opt_pricer.delta(*arg_list)
            except ValueError:
                results[ret_type] = 0
                results['ERROR'].append('Error calculating delta')
        elif ret_type == ReturnTypes.GAMMA:
            try:
                results[ret_type] = opt_pricer.gamma(*arg_list)
            except ValueError:
                results[ret_type] = 0
                results['ERROR'].append('Error calculating gamma')
        elif ret_type == ReturnTypes.THETA:
            try:
                results[ret_type] = opt_pricer.theta(*arg_list)
            except ValueError:
                results[ret_type] = 0
                results['ERROR'].append('Error calculating theta')
        elif ret_type == ReturnTypes.VEGA:
            try:
                results[ret_type] = opt_pricer.vega(*arg_list)
            except ValueError:
                results[ret_type] = 0
                results['ERROR'].append('Error calculating vega')
        elif ret_type == ReturnTypes.RHO:
            try:
                results[ret_type] = opt_pricer.rho(*arg_list)
            except ValueError:
                results[ret_type] = 0
                results['ERROR'].append('Error calculating rho')
        else:
            results['ERROR'].append('Incorrect return type provided')

    return jsonify(results)
