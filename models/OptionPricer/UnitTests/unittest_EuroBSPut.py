import unittest

from models.OptionPricer.EuropeanBSPutOptionPricer import EuropeanBSPutOptionPricer


class TestEuroBSPut(unittest.TestCase):
    def test_Pricer_instance(self):
        P = EuropeanBSPutOptionPricer()
        self.assertIsInstance(P, EuropeanBSPutOptionPricer)

    def test_Pricer_d1(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P._d1(100, 100, 0, 1, 0.2, 0.05), 0.35, 6)

    def test_Pricer_d2(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P._d2(100, 100, 0, 1, 0.2, 0.05), 0.15, 6)

    def test_Pricer_value(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.value(100, 100, 0, 1, 0.2, 0.05, 'B'), 5.5735, 4)

    def test_Pricer_value_T0(self):
        P = EuropeanBSPutOptionPricer()
        # T = 0
        self.assertAlmostEqual(P.value(90, 100, 0, 0, 0.2, 0.05, 'B'), 10, 4)

    def test_Pricer_value_S0(self):
        P = EuropeanBSPutOptionPricer()
        # S = 0
        self.assertAlmostEqual(P.value(0, 100, 0, 1, 0.2, 0.05, 'B'), 95.1229, 4)

    def test_Pricer_value_K0(self):
        P = EuropeanBSPutOptionPricer()
        # K = 0
        self.assertAlmostEqual(P.value(100, 0, 0, 1, 0.2, 0.05, 'B'), 0, 4)

    def test_Pricer_value_Sneg(self):
        P = EuropeanBSPutOptionPricer()
        # S < 0
        with self.assertRaises(ValueError):
            P.value(-100, 100, 0, 1, 0.2, 0.05, 'B')

    def test_Pricer_delta(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.delta(100, 100, 0, 1, 0.2, 0.05, 'B'), -0.3632, 4)

    def test_Pricer_gamma(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.gamma(100, 100, 0, 1, 0.2, 0.05, 'B'), 0.0188, 4)

    def test_Pricer_theta(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.theta(100, 100, 0, 1, 0.2, 0.05, 'B'), -1.6579, 4)

    def test_Pricer_vega(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.vega(100, 100, 0, 1, 0.2, 0.05, 'B'), 37.524, 4)

    def test_Pricer_rho(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.rho(100, 100, 0, 1, 0.2, 0.05, 'B'), -41.8905, 4)

    def test_Pricer_rho_sell(self):
        P = EuropeanBSPutOptionPricer()
        self.assertAlmostEqual(P.rho(100, 100, 0, 1, 0.2, 0.05, 'S'), 41.8905, 4)

if __name__ == '__main__':
    unittest.main()
