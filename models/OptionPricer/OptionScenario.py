from models.OptionPricer.OptionStrategy import OptionStrategy
from models.OptionPricer.CalcType import CalcType
from models.OptionPricer.X_Type import X_Type


class OptionScenario(object):
    """
    Define option scenarios and calculate the value and greeks.
    """

    def __init__(self):
        """
        Initialise an instance of the class

        :return: n/a
        """

        self.OptStrategy = OptionStrategy()

    def calculate(self, strategy_list, y_type, x_type, xmin, xmax, num_points=10, time_offset=0,
                  include_original_x_vals=False, add_support_points=False):
        """
        Calculate the option value or greek over the range of values specified in the arguments

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell],
                                  ('P', [S, K, D, T, sigma, r, buy_sell]]
                              where 'C' denotes a Call option and 'P' a Put option.
        :param y_type: the dependent variable that is being calculated.
        :param x_type: the independent variable that is being modified.
        :param xmin: the minimum value for the x_type
        :param xmax: the maximum value for the x_type
        :param num_points: the number of points between xmin and xmax where calculation is done. Default is 10 points.
        :param time_offset: calculate the value/greek of scenario at given time epoch. default offset is 0. i.e.
                            the time is (T-0). That means the value of the Option is calculate at present time.
                            To calculate the value of the Option at expiry (e.g. for a T=1 year Option), set the
                            time_offset = 1. So, the time is now (T-1), that is at expiry.
        :param include_original_x_vals: include the original x-values given in the strategy_list, in the calculation.
                                        They will be included if original x-values are in the range (xmin, xmax).
        :param add_support_points: Add points on either side of the actual x value in the strategy to support it and
                                   provide better accuracy. If include_include_original_x_vals is False, this option
                                   will not be activated.

        :return: a tuple containing 1) the scenario total result, 2) the scenario leg results, 3) the x values used
        """

        # find vector of num_points [xmin, xmax]
        # calculate the option strategy for each point:
        #   override x_type
        #   calculate y_type
        # return results

        x_vals = self._get_x_vector(xmin, xmax, num_points, strategy_list, x_type,
                                    include_original_x_vals, add_support_points)
        fn = self._lookup_strategy_formula(y_type)

        scen_total_result = list()
        scen_leg_result = list()

        for x in x_vals:
            new_strategy_list = self._adjust_leg_params(strategy_list, x_type, x, time_offset)
            total_res, leg_res = fn(new_strategy_list)

            # Update the scenario list
            scen_total_result.append(total_res)
            scen_leg_result.append(leg_res)

        return scen_total_result, scen_leg_result, list(x_vals)

    def _lookup_strategy_formula(self, type_id):
        """
        Lookup the strategy to run for a given type
        :param type_id: the type of strategy. value or a greek.
        :return: returns a function corresponding to the strategy.
        """

        if type_id == CalcType.VALUE:
            return self.OptStrategy.value

        elif type_id == CalcType.DELTA:
            return self.OptStrategy.delta

        elif type_id == CalcType.GAMMA:
            return self.OptStrategy.gamma

        elif type_id == CalcType.THETA:
            return self.OptStrategy.theta

        elif type_id == CalcType.VEGA:
            return self.OptStrategy.vega

        elif type_id == CalcType.RHO:
            return self.OptStrategy.rho

        else:
            raise ValueError('Incorrect valuation type provided.')

    def _adjust_leg_params(self, strategy_list, x_type, x, time_offset):
        """
        Adjust the strategy inputs according to the required scenario.
        For example, if the scenario is to calculate the option value at different underlying asset values,
        then, the parameter 'S' of each leg of the strategy is adjusted.

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell],
                                  ('P', [S, K, D, T, sigma, r, buy_sell]]
                              where 'C' denotes a Call option and 'P' a Put option.
        :param x_type: the independent variable that is being modified.
        :param x: the value of the x_type variable to apply.
        :param time_offset: time epoch to calculate value/greek of Option.

        :return: the adjusted strategy list.
        """

        # split the values in the strategy list, for each strategy, modify the relevant value and pack the strategy
        # list again.

        new_strategy_list = list()
        
        for i, leg in enumerate(strategy_list):

            try:
                new_leg = self.OptStrategy.update_strategy(leg, x_type, x)
            except ValueError as e:
                print(str(e))
                new_leg = leg

            # adjust the time to expiry
            # if the x_type is not time T (as time is already being varied)
            # if the time offset is after the option expiry, then set option value to 0
            if x_type != X_Type.T:

                time_to_expiry = self.OptStrategy.get_strategy_param(new_leg, X_Type.T)
                if time_offset <= time_to_expiry:
                    new_leg = self.OptStrategy.update_strategy(new_leg, X_Type.T, time_to_expiry - time_offset)
                else:
                    # S = 0, K = 0
                    new_leg = self.OptStrategy.update_strategy(new_leg, X_Type.S, 0)
                    new_leg = self.OptStrategy.update_strategy(new_leg, X_Type.K, 0)

            new_strategy_list.append(new_leg)

        return new_strategy_list

    def _get_x_vector(self, xmin, xmax, num_points, strategy_list, x_type,
                      include_original_x_vals=False,  add_points_around_original_x=False):
        """
        Divide the space between xmin and xmax into num_points. The original value for each leg
        is also included in the vector, if they are within the range of the min and max values.

        :param xmin: the minimum value for the x_type
        :param xmax: the maximum value for the x_type
        :param num_points: the number of points between xmin and xmax where calculation is done.
        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell],
                                  ('P', [S, K, D, T, sigma, r, buy_sell]]
                              where 'C' denotes a Call option and 'P' a Put option.
        :param x_type: the independent variable that is being modified.
        :param add_points_around_original_x: if the original value of each leg is in the range of (xmin, xmax), then two
                                             points are included on either side to provide accuracy to the calculation
                                             around the value. True, by default.


        if num_points is negative, an empty list is returned.
        if num_points is zero, an empty list is returned.
        if xmin > xmax, the values are reordered and the list is returned.

        :return: a list of values
        """

        def value_in_range(z, x, y):
            """
            Check if x <= z <=y, which will return True, otherwise False
            """

            if (z >= x) and (z <= y):
                return True
            else:
                return False


        x_vec = list()
        if (num_points < 0) or (num_points == 0):
            return x_vec

        # if xmin > xmax, the direction should be reversed
        if xmin > xmax:
            xmin, xmax = xmax, xmin

        # find the spacing between points
        dx = (xmax - xmin) / (num_points - 1)

        x_vec.append(xmin)
        last_x = x_vec[-1]

        while (last_x + dx) <= xmax + 1e-6:
            x_vec.append(last_x + dx)
            last_x = x_vec[-1]

        # add the original points in the strategy to the x vector, if requested
        if include_original_x_vals:
            for i, leg in enumerate(strategy_list):

                try:
                    orig_val = self.OptStrategy.get_strategy_param(leg, x_type)
                    if value_in_range(orig_val, xmin, xmax):
                        x_vec.append(orig_val)

                    # if adding extra points around the original x value is enabled
                    if add_points_around_original_x:
                        # if the original value in the range of (xmin, xmax)
                        if value_in_range(orig_val, xmin, xmax):
                            # add points at (x - 1% x) and (x + 1% x)
                            x_vec.append(orig_val * 0.99)
                            x_vec.append(orig_val * 1.01)

                except ValueError as e:
                    print(str(e))

        # get the unique values and sort the vector
        x_vec = list(set(x_vec))
        x_vec.sort()

        return x_vec
