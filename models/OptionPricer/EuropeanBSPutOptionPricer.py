from math import exp
from math import sqrt

from models.OptionPricer.BaseBSOptionPricer import BaseBSOptionPricer


class EuropeanBSPutOptionPricer(BaseBSOptionPricer):
    """
    A European Put Option using the Black-Scholes pricing model
    """

    def __init__(self):
        """
        Initialise an instance of the class

        :return: N/A
        """

        # initialise the option parameters
        super(EuropeanBSPutOptionPricer, self).__init__()

    def value(self, S, K, D, T, sigma, r, buy_sell):
        """
        European Put Option payoff

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: the option payoff
        """

        # check the input arguments and raise an exception if they are incorrect
        self._check_pricer_inputs(S, K, T, sigma)

        d1 = self._d1(S, K, D, T, sigma, r)
        d2 = self._d2(S, K, D, T, sigma, r)

        val = -1 * S * exp(-1 * D * T) * self._get_CDF_norm(-1 * d1) \
              + K * exp(-1 * r * T) * self._get_CDF_norm(-1 * d2)

        return max(val, 0) * self._buy_sell_sign(buy_sell)

    def delta(self, S, K, D, T, sigma, r, buy_sell):
        """
        Delta of a European Put option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: Delta of the option
        """

        # check the input arguments and raise an exception if they are incorrect
        self._check_pricer_inputs(S, K, T, sigma)

        d1 = self._d1(S, K, D, T, sigma, r)
        return exp(-1 * D * T) * (self._get_CDF_norm(d1) - 1) * self._buy_sell_sign(buy_sell)

    def gamma(self, S, K, D, T, sigma, r, buy_sell):
        """
        Gamma of a European Put option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: Gamma of the option
        """

        # check the input arguments and raise an exception if they are incorrect
        self._check_pricer_inputs(S, K, T, sigma)

        # check if time to expiry T=0
        if T == 0:
            T = 1e-20

        # check if volatility sigma=0
        if sigma == 0:
            sigma = 1e-20

        # check if price S=0
        if S == 0:
            S = 1e-20

        d1 = self._d1(S, K, D, T, sigma, r)

        part1 = exp(-1 * D * T) * self._get_CDF_norm_derivative(d1)
        part2 = sigma * S * sqrt(T)

        try:
            return part1/part2 * self._buy_sell_sign(buy_sell)
        except:
            raise ValueError

    def theta(self, S, K, D, T, sigma, r, buy_sell):
        """
        Theta of a European Put option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: Theta of the option
        """

        # check the input arguments and raise an exception if they are incorrect
        self._check_pricer_inputs(S, K, T, sigma)

        # check if time to expiry T=0
        if T == 0:
            T = 1e-20

        d1 = self._d1(S, K, D, T, sigma, r)
        d2 = self._d2(S, K, D, T, sigma, r)

        part1 = -1 * sigma * S * exp(-1 * D * T) * self._get_CDF_norm_derivative(-1 * d1)
        part2 = 2 * sqrt(T)
        part3 = -1 * D * S * self._get_CDF_norm(-1 * d1) * exp(-1 * D * T)
        part4 = r * K * exp(-1 * r * T) * self._get_CDF_norm(-1 * d2)

        return (part1/part2 + part3 + part4) * self._buy_sell_sign(buy_sell)

    def vega(self, S, K, D, T, sigma, r, buy_sell):
        """
        Vega of a European Put option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: Vega of the option
        """

        # check the input arguments and raise an exception if they are incorrect
        self._check_pricer_inputs(S, K, T, sigma)

        d1 = self._d1(S, K, D, T, sigma, r)

        return S * sqrt(T) * exp(-1 * D * T) * self._get_CDF_norm_derivative(d1) * self._buy_sell_sign(buy_sell)

    def rho(self, S, K, D, T, sigma, r, buy_sell):
        """
        Rho of a European Put option

        :param buy_sell: Buy or sell option. Specify as 'B' or 'S'.
        :param S: underlying asset price
        :param K: Option strike price
        :param D: dividend yield
        :param T: time to expiry
        :param sigma: the volatility of the underlying
        :param r: the interest rate

        :return: Rho of the option
        """

        # check the input arguments and raise an exception if they are incorrect
        self._check_pricer_inputs(S, K, T, sigma)

        d2 = self._d2(S, K, D, T, sigma, r)

        return -1 * K * T * exp(-1 * r * T) * self._get_CDF_norm(-1 * d2) * self._buy_sell_sign(buy_sell)