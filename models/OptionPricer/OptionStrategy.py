from copy import deepcopy

from models.OptionPricer.EuropeanBSCallOptionPricer import EuropeanBSCallOptionPricer
from models.OptionPricer.EuropeanBSPutOptionPricer import EuropeanBSPutOptionPricer
from models.OptionPricer.X_Type import X_Type


class OptionStrategy(object):
    """
    Define option strategies

    """

    def __init__(self):
        """
        Initialise an instance of the class

        :return: n/a
        """

        # create European Call / Put pricers
        self.COpt = EuropeanBSCallOptionPricer()
        self.POpt = EuropeanBSPutOptionPricer()

    def _process_strategy(self, strategy_list, func_dict):
        """
        private method to process each leg of the strategy

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.
        :param func_dict: a dictionary of functions to apply to each leg

        :return: a tuple of the total strategy value, and a list of the value of each leg.
        :raises ValueError: On error, ValueError is raised.
        """

        # empty list of strategies
        if len(strategy_list) == 0:
            return 0, 0

        results = list()
        total = 0

        for leg in strategy_list:
            leg_type = leg[0].upper()
            leg_params = leg[1]

            try:
                if leg_type in func_dict.keys():
                    v = func_dict[leg_type](*leg_params)
                else:
                    # undefined option type
                    v = 0
            except ValueError as e:
                print(str(e))
                # v = 0
                raise

            total += v
            results.append(v)

        return total, results

    def value(self, strategy_list):
        """
        Calculate the value of a strategy (group of options)

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.

        :return: a tuple of the total strategy value, and a list of the value of each leg
        """

        # dictionary of valuation functions
        func_dict = dict({'C': self.COpt.value, 'P': self.POpt.value})
        total, results = self._process_strategy(strategy_list, func_dict)

        return total, results

    def delta(self, strategy_list):
        """
        Calculate the delta of a strategy (group of options)

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.

        :return: a tuple of the total strategy value, and a list of the value of each leg
        """

        # dictionary of valuation functions
        func_dict = dict({'C': self.COpt.delta, 'P': self.POpt.delta})
        total, results = self._process_strategy(strategy_list, func_dict)

        return total, results

    def gamma(self, strategy_list):
        """
        Calculate the gamma of a strategy (group of options)

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.

        :return: a tuple of the total strategy value, and a list of the value of each leg
        """

        # dictionary of valuation functions
        func_dict = dict({'C': self.COpt.gamma, 'P': self.POpt.gamma})
        total, results = self._process_strategy(strategy_list, func_dict)

        return total, results

    def theta(self, strategy_list):
        """
        Calculate the rho of a strategy (group of options)

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.

        :return: a tuple of the total strategy value, and a list of the value of each leg
        """

        # dictionary of valuation functions
        func_dict = dict({'C': self.COpt.theta, 'P': self.POpt.theta})
        total, results = self._process_strategy(strategy_list, func_dict)

        return total, results

    def vega(self, strategy_list):
        """
        Calculate the rho of a strategy (group of options)

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.

        :return: a tuple of the total strategy value, and a list of the value of each leg
        """

        # dictionary of valuation functions
        func_dict = dict({'C': self.COpt.vega, 'P': self.POpt.vega})
        total, results = self._process_strategy(strategy_list, func_dict)

        return total, results

    def rho(self, strategy_list):
        """
        Calculate the rho of a strategy (group of options)

        :param strategy_list: a list of tuples containing option pricing arguments in this form:
                                [ ('C', [S, K, D, T, sigma, r, buy_sell]),
                                  ('P', [S, K, D, T, sigma, r, buy_sell])]
                              where 'C' denotes a Call option and 'P' a Put option.

        :return: a tuple of the total strategy value, and a list of the value of each leg
        """

        # dictionary of valuation functions
        func_dict = dict({'C': self.COpt.rho, 'P': self.POpt.rho})
        total, results = self._process_strategy(strategy_list, func_dict)

        return total, results

    def get_strategy_param(self, strategy, param_name):
        """
        Get a strategy parameter.

        :param strategy: a tuple containing option pricing arguments in this form:
                                ('C', [S, K, D, T, sigma, r, buy_sell])
                                or
                                ('P', [S, K, D, T, sigma, r, buy_sell])
                              where 'C' denotes a Call option and 'P' a Put option.
        :param param_name: a parameter name given as a X_Type class, e.g. X_Type.S

        :return: the parameter value
        """

        if param_name == X_Type.S:
            val = strategy[1][0]

        elif param_name == X_Type.K:
            val = strategy[1][1]

        elif param_name == X_Type.D:
            val = strategy[1][2]

        elif param_name == X_Type.T:
            val = strategy[1][3]

        elif param_name == X_Type.SIGMA:
            val = strategy[1][4]

        elif param_name == X_Type.R:
            val = strategy[1][5]
        else:
            raise ValueError('Incorrect valuation type provided.')

        return val

    def update_strategy(self, strategy, param_name, param_value):
        """
        Update a strategy parameter

        :param strategy: a tuple containing option pricing arguments in this form:
                                ('C', [S, K, D, T, sigma, r, buy_sell])
                                or
                                ('P', [S, K, D, T, sigma, r, buy_sell])
                              where 'C' denotes a Call option and 'P' a Put option.
        :param param_name: a parameter name given as a X_Type class, e.g. X_Type.S
        :param param_value: the new value of the parameter

        :return: a strategy tuple, with the updated parameter
        """

        # create a deep copy, as the original values will be modified
        new_strategy = deepcopy(strategy)

        if param_name == X_Type.S:
            new_strategy[1][0] = param_value

        elif param_name == X_Type.K:
            new_strategy[1][1] = param_value

        elif param_name == X_Type.D:
            new_strategy[1][2] = param_value

        elif param_name == X_Type.T:
            new_strategy[1][3] = param_value

        elif param_name == X_Type.SIGMA:
            new_strategy[1][4] = param_value

        elif param_name == X_Type.R:
            new_strategy[1][5] = param_value
        else:
            raise ValueError('Incorrect valuation type provided.')

        return new_strategy
