import unittest

from models.OptionPricer.OptionScenario import OptionScenario
from models.OptionPricer.CalcType import CalcType
from models.OptionPricer.X_Type import X_Type


class TestOptionScenario(unittest.TestCase):
    def test_scenario_instance(self):
        S = OptionScenario()
        self.assertIsInstance(S, OptionScenario)

    def test_lookup_VALUE(self):
        S = OptionScenario()
        fn = S.OptStrategy.value
        self.assertEqual(S._lookup_strategy_formula(CalcType.VALUE), fn)

    def test_lookup_DELTA(self):
        S = OptionScenario()
        fn = S.OptStrategy.delta
        self.assertEqual(S._lookup_strategy_formula(CalcType.DELTA), fn)

    def test_lookup_GAMMA(self):
        S = OptionScenario()
        fn = S.OptStrategy.gamma
        self.assertEqual(S._lookup_strategy_formula(CalcType.GAMMA), fn)

    def test_lookup_THETA(self):
        S = OptionScenario()
        fn = S.OptStrategy.theta
        self.assertEqual(S._lookup_strategy_formula(CalcType.THETA), fn)
        
    def test_lookup_VEGA(self):
        S = OptionScenario()
        fn = S.OptStrategy.vega
        self.assertEqual(S._lookup_strategy_formula(CalcType.VEGA), fn)

    def test_lookup_RHO(self):
        S = OptionScenario()
        fn = S.OptStrategy.rho
        self.assertEqual(S._lookup_strategy_formula(CalcType.RHO), fn)

    def test_adjust_param(self):
        S = OptionScenario()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        leg1 = ('C', [110, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [110, 100, 0, 1, 0.2, 0.05, 'S'])
        strat_modif = [leg1, leg2]

        x_type = X_Type.S
        x = 110
        tau = 0

        self.assertEqual(S._adjust_leg_params(strat, x_type, x, tau), strat_modif)

    def test_adjust_param2(self):
        S = OptionScenario()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        leg1 = ('C', [110, 100, 0, 0.7, 0.2, 0.05, 'B'])
        leg2 = ('P', [110, 100, 0, 0.7, 0.2, 0.05, 'S'])
        strat_modif = [leg1, leg2]

        x_type = X_Type.S
        x = 110
        tau = 0.3

        self.assertEqual(S._adjust_leg_params(strat, x_type, x, tau), strat_modif)

    def test_adjust_param3(self):
        S = OptionScenario()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        leg1 = ('C', [0, 0, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [0, 0, 0, 1, 0.2, 0.05, 'S'])
        strat_modif = [leg1, leg2]

        x_type = X_Type.S
        x = 110
        tau = 1.3

        self.assertEqual(S._adjust_leg_params(strat, x_type, x, tau), strat_modif)

    def test_adjust_param4(self):
        S = OptionScenario()
        leg1 = ('C', [100, 100, 0, 2, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        leg1 = ('C', [110, 100, 0, 0.7, 0.2, 0.05, 'B'])
        leg2 = ('P', [0, 0, 0, 1, 0.2, 0.05, 'S'])
        strat_modif = [leg1, leg2]

        x_type = X_Type.S
        x = 110
        tau = 1.3

    def test_adjust_param5(self):
        S = OptionScenario()
        leg1 = ('C', [100, 100, 0, 2, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        leg1 = ('C', [100, 100, 0, 0.7, 0.5, 0.05, 'B'])
        leg2 = ('P', [0, 0, 0, 1, 0.5, 0.05, 'S'])
        strat_modif = [leg1, leg2]

        x_type = X_Type.SIGMA
        x = 0.5
        tau = 1.3

        self.assertEqual(S._adjust_leg_params(strat, x_type, x, tau), strat_modif)

    def test_calculate(self):
        S = OptionScenario()
        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        y_type = CalcType.VALUE
        x_type = X_Type.S
        xmin = 90
        xmax = 110
        num_points = 3
        tau = 0

        answers = [5.0912 + -10.214165, 10.4506 + -5.5735, 17.6630 + -2.7859]

        scen_total, scen_leg_res, xvals = S.calculate(strat, y_type, x_type, xmin, xmax, num_points, tau)

        for i, val in enumerate(scen_total):
            self.assertAlmostEqual(val, answers[i], 4)

    def test_calculate2(self):
        S = OptionScenario()
        leg1 = ('C', [100, 105, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1]

        y_type = CalcType.VALUE
        x_type = X_Type.S
        xmin = 90
        xmax = 110
        num_points = 3
        tau = 1

        # answers = [0 - 10, 0 - 0, 5 - 0]
        answers = [0, 0, 5]

        scen_total, scen_leg_res, xvals = S.calculate(strat, y_type, x_type, xmin, xmax, num_points, tau)

        for i, val in enumerate(scen_total):
            self.assertAlmostEqual(val, answers[i], 4)

    def test_xvals(self):
        S = OptionScenario()
        xmin = 90
        xmax = 110
        num_points = 3

        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.S

        answers = [90.0, 99, 100.0, 101, 110.0]

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type, True, True), answers)

    def test_xvals2(self):
        S = OptionScenario()
        xmin = 110
        xmax = 90
        num_points = 3

        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.S

        answers = [90.0, 99, 100.0, 101, 110.0]

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type, True, True), answers)

    def test_xvals3(self):
        S = OptionScenario()
        xmin = 110
        xmax = 90
        num_points = 0

        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.S

        answers = []

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type), answers)

    def test_xvals4(self):
        S = OptionScenario()
        xmin = 110
        xmax = 90
        num_points = -3

        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.S

        answers = []

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type), answers)

    def test_xvals5(self):
        S = OptionScenario()
        xmin = 90
        xmax = 110
        num_points = 5

        leg1 = ('C', [100, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.S

        answers = [90, 95, 100, 105, 110]

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type, False), answers)

    def test_xvals6(self):
        S = OptionScenario()
        xmin = 90
        xmax = 110
        num_points = 5

        leg1 = ('C', [108, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.S

        answers = [90, 95, 99, 100, 101, 105, 106.92, 108, 109.08, 110]

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type, True, True), answers)

    def test_xvals7(self):
        S = OptionScenario()
        xmin = 0
        xmax = 0.5
        num_points = 5

        leg1 = ('C', [108, 100, 0, 1, 0.2, 0.05, 'B'])
        leg2 = ('P', [100, 100, 0, 1, 0.2, 0.05, 'S'])
        strat = [leg1, leg2]

        x_type = X_Type.SIGMA

        answers = [0.0, 0.125, 0.198, 0.2, 0.202, 0.25, 0.375, 0.5]

        self.assertEqual(S._get_x_vector(xmin, xmax, num_points, strat, x_type, True, True), answers)

if __name__ == '__main__':
    unittest.main()
